<%@page import="cl.model.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Calculadora c1 = (Calculadora)request.getAttribute("calculadora");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
<body>
    <h1>Resultado Calculo de interés</h1>
    <br>
    <br>
    Capital:
    <%= c1.getC() %>
    <br>
    <br>
    Tasa interés anual:
    <%= c1.getI() %>
    <br>
    <br>
    Nº de Años:
    <%= c1.getA() %>
    <br>
    <br>
    Interés Anual:
    <%= c1.CalcularInteres() %>

</body>
</html>
